<?php

namespace App\Http\Controllers\Formfield;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DOMDocument;
use Illuminate\Support\Facades\Input;

class SummernoteController extends Controller
{
    //
    public function upload(Request $request)
    {

        $image=$request->file('file');
        $new_name = rand() .'.'.$image->getClientOriginalExtension();
        $image->move(public_path('images/'), $new_name);
        $file_name = url('images/') . '/'. $new_name;
        return $file_name;

    }
}
