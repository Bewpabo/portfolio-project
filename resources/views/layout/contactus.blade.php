@extends('layout.master')

@section('content')
<!-- <div class="woe fadeIn">
    <h3 class="h3 text-center mb-3 mt-3">{{ $page->title }}</h3>

</div>
<hr class="mb-5">
 -->
<div class="col-md-12 col-md-offset-2">



    <!--Grid column-->



    <div class="view">
        <img src="{{ Voyager::image( $page->image ) }}" class="img-fluid" alt="smaple image">

        <div class="mask contact-us mt-5">
            <h3 class="h3 text-center mb-3 mt-3">{{ $page->title }}</h3>
            <p class="black-text">{!! $page->body !!}</p>
        </div>
    </div>

</div>




@endsection
