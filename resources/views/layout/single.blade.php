@extends('layout.master')

@section('content')

<div class="row single">
    <!--   <div class="col align-self-start">
        One of three columns
    </div>
    <div class="col align-self-center">
        One of three columns
    </div>
    <div class="col align-self-end">
        One of three columns
    </div> -->

    <!--    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <h3 class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" role="tab" aria-controls="nav-home"
                aria-selected="true">
                {{ $portfolio->title }}
            </h3>

        </div>
    </nav> -->


    <div class="col-md-12 col-md-offset-2">
        <div class="tab-content" id="nav-tabContent">
            <div class="text-center">


                <div class="woe fadeIn">
                    <h3 class="h3 text-center mb-3 mt-3">{{ $portfolio->title }}</h3>

                </div>
                <img src="{{ Voyager::image( $portfolio->image ) }}" class="img-content z-depth-4 rounded">

            </div>
            <div class="mt-3">
                <h1>{{ $portfolio->title }}</h1>
            </div>
            <hr class="mb-5">
            <p>{!! $portfolio->body !!}</p>
        </div>
    </div>

</div>

@endsection
