@extends('layout.master')

@section('content')

<div class="row single">



    <div class="col-md-12 col-md-offset-2">
        <div class="tab-content" id="nav-tabContent">
            <div class="text-center">


                <div class="woe fadeIn">
                    <h3 class="h3 text-center mb-3 mt-3">{{ $article->title }}</h3>

                </div>
                <img src="{{ Voyager::image( $article->image ) }}" class="img-content z-depth-4 rounded">

            </div>
            <div class="mt-3">
                <h1>{{ $article->title }}</h1>
            </div>
            <hr class="mb-5">
            <p>{!! $article->body !!}</p>
        </div>
    </div>

</div>

@endsection
