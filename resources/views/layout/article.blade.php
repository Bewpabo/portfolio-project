@extends('layout.master')

@section('content')
<div class="woe fadeIn">
    <h3 class="h3 text-center mb-3 mt-3">Article</h3>

</div>
<hr class="mb-5">
@foreach($articles as $article)
<div class="row wow fadeIn">

    <!--Grid column-->
    <div class="col-lg-5 col-xl-4 mb-4">
        <!--Featured image-->
        <div class="view overlay rounded z-depth-1-half">
            <div class="view overlay">
                <img src="{{ Voyager::image( $article->image ) }}" style="width:100%">
            </div>
        </div>
    </div>
    <!--Grid column-->

    <!--Grid column-->
    <div class="col-lg-7 col-xl-7 ml-xl-4 mb-4">
        <h3 class="mb-3 font-weight-bold dark-grey-text">
            <strong>{{ $article->title }}</strong>
        </h3>
        <p class="grey-text">{{ $article->excerpt }}</p>

        <a href="{{ url('article/'.$article->slug)}}" target="_blank" class="btn btn-primary btn-md">View more
            <i class="fas fa-play ml-2"></i>
        </a>
    </div>
    <!--Grid column-->
</div>
<!--Grid row-->
<hr class="mb-5">



@endforeach

<!--Pagination-->
<nav class="d-flex justify-content-center wow fadeIn">
    {{ $articles->links() }}
</nav>
<!--Pagination-->


<!--Section: Cards-->

@endsection
